package dawson;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    
    @Test
    public void echo_returns_five(){
        App a = new App();
        assertEquals("This test wants to check if echo returns 5", 5, a.echo(5));
    }
    @Test
    public void oneMore_succeeds(){
        App a = new App();
        assertEquals("One more method should add one more to x", 6, a.oneMore(5));
    }
}
